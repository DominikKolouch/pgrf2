package raster;

public class DepthBuffer implements Raster<Double> {
    private final double[][] buffer;
    private final int width, height;
    private double clearValue = 1;

    public DepthBuffer(int width, int height) {
        this.buffer = new double[width][height];
        this.width = width;
        this.height = height;
        for (int i = 0; i < width; ++i) {
            for (int j = 0; j < height; ++j) {
                buffer[i][j] = 1;
            }
        }
    }

    @Override
    public void clear() {
        for (int i = 0; i < width; ++i) {
            for (int j = 0; j < height; ++j) {
                buffer[i][j] = 1;
            }
        }
    }

    @Override
    public void setClearValue(Double value) {
        this.clearValue = value;
    }

    @Override
    public int getWidth() {
        return this.width;
    }

    @Override
    public int getHeight() {
        return this.height;
    }

    @Override
    public Double getElement(int x, int y) {
        if (checkBorders(x, y))
            return buffer[x][y];
        else return 0.;
    }

    @Override
    public void setElement(int x, int y, Double value) {
        if (checkBorders(x, y))
            buffer[x][y] = value;
    }


}
