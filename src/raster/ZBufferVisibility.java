package raster;

import transforms.Col;

public class ZBufferVisibility {
    private final ImageBuffer iBuffer;
    private final DepthBuffer dBuffer;

    public ZBufferVisibility(ImageBuffer iBuffer) {
        this.iBuffer = iBuffer;
        // TODO: init depth bufferu
        this.dBuffer = new DepthBuffer(iBuffer.getWidth(), iBuffer.getHeight());

    }

    public void drawPixelWithTest(int x, int y, double z, Col color) {
        if (z < dBuffer.getElement(x, y)) {
            dBuffer.setElement(x, y, z);
            iBuffer.setElement(x, y, color);
        }
    }

    public ImageBuffer getiBuffer() {
        return iBuffer;
    }

    public void clearDepthBuffer() {
        dBuffer.clear();
    }
}
