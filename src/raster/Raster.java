package raster;

public interface Raster<E> {

    void clear();

    void setClearValue(E value);

    int getWidth();

    int getHeight();

    E getElement(int x, int y);

    void setElement(int x, int y, E value);

    default boolean checkBorders(int x, int y) {
        return (x >= 0 && x <= getWidth() - 1 && y >= 0 && y <= getHeight() - 1);
    }
}
