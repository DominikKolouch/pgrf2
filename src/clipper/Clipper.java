package clipper;

import model.Vertex;
import transforms.Point3D;

public class Clipper {

    public Clipper() {

    }

    public boolean triangleClip(Vertex v1, Vertex v2, Vertex v3) {
        //fast clip
        double aw, ax, ay, az, bx, by, bz, bw, cy, cz, cx, cw;
        Point3D posA = v1.getPosition();
        Point3D posB = v2.getPosition();
        Point3D posC = v3.getPosition();
        ax = posA.getX();
        ay = posA.getY();
        az = posA.getZ();
        aw = posA.getW();
        bx = posB.getX();
        by = posB.getY();
        bz = posB.getZ();
        bw = posB.getW();
        cx = posC.getX();
        cy = posC.getY();
        cz = posC.getZ();
        cw = posC.getW();


        return ((ax < -aw && bx < -bw && cx < -cw) ||
                (ax > aw && bx > bw && cx > cw) ||
                (ay < -aw && by < -bw && cy < -cw) ||
                (ay > aw && by > bw && cy > cw) ||
                (az < 0 && bz < 0 && cz < 0) ||
                (az > aw && bz > bw && cz > cw));

    }

    public boolean lineClip(Vertex v1, Vertex v2) {
        double aw, ax, ay, az, bx, by, bz, bw;
        Point3D posA = v1.getPosition();
        Point3D posB = v2.getPosition();
        ax = posA.getX();
        ay = posA.getY();
        az = posA.getZ();
        aw = posA.getW();
        bx = posB.getX();
        by = posB.getY();
        bz = posB.getZ();
        bw = posB.getW();
        return ((ax < -aw && bx < -bw) ||
                (ax > aw && bx > bw) ||
                (ay < -aw && by < -bw) ||
                (ay > aw && by > bw) ||
                (az < 0 && bz < 0) ||
                (az > aw && bz > bw));

    }

    public boolean clipPoint(Vertex v1) {
        double aw, ax, ay, az;
        Point3D posA = v1.getPosition();
        ax = posA.getX();
        ay = posA.getY();
        az = posA.getZ();
        aw = posA.getW();
        return ((ax < -aw) ||
                (ax > aw) ||
                (ay < -aw) ||
                (ay > aw) ||
                (az < 0) ||
                (az > aw));
    }
}
