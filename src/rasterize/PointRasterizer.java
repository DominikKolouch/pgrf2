package rasterize;

import model.Vertex;
import raster.ZBufferVisibility;
import shaders.Shader;
import transforms.Vec3D;

public class PointRasterizer  extends  Rasterizer{


    public PointRasterizer(ZBufferVisibility zBufferVisibility, Shader shader) {
        super(zBufferVisibility, shader);
    }

    public void rasterizer(Vertex vec)
    {
        vec = vec.mul(1/vec.getW());
        Vec3D v1=transToWindows(vec);
        double x,y,z;
        x = v1.getX();
        y = v1.getY();
        z = v1.getZ();
        zBufferVisibility.drawPixelWithTest((int)x,(int)y,z,vec.getColor());
    }


}
