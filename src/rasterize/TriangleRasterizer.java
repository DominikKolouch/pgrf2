package rasterize;

import model.Vertex;
import raster.ZBufferVisibility;
import shaders.Shader;
import transforms.Vec3D;

public class TriangleRasterizer extends Rasterizer {


    public TriangleRasterizer(ZBufferVisibility zBufferVisibility, Shader shader) {
        super(zBufferVisibility, shader);
    }

    public void rasterizer(Vertex v1, Vertex v2, Vertex v3) {

        v1 = v1.mul(1 / v1.getW());
        v2 = v2.mul(1 / v2.getW());
        v3 = v3.mul(1 / v3.getW());


        Vec3D a = transToWindows(v1);
        Vec3D b = transToWindows(v2);
        Vec3D c = transToWindows(v3);

        //todo seřadit vrcholy podle y
        Vec3D temp;
        Vertex pom;

        if (b.getY() < a.getY()) {
            temp = a;
            a = b;
            b = temp;

            pom = v1;
            v1 = v2;
            v2 = pom;
        }
        if (c.getY() < b.getY()) {
            temp = b;
            b = c;
            c = temp;

            pom = v2;
            v2 = v3;
            v3 = pom;
        }
        if (b.getY() < a.getY()) {
            temp = a;
            a = b;
            b = temp;

            pom = v1;
            v1 = v2;
            v2 = pom;
        }


        for (int y = (int) Math.max(a.getY() + 1, 0); y <= Math.min(b.getY(), height - 1); y++) {
            double s1 = (y - a.getY()) / (b.getY() - a.getY());
            double x1 = (a.getX() * (1 - s1) + b.getX() * s1);

            Vertex v12 = v1.mul(1 - s1).add(v2.mul(s1));

            double s2 = (y - a.getY()) / (c.getY() - a.getY());
            double x2 = (a.getX() * (1 - s2) + c.getX() * s2);

            Vertex v13 = v1.mul(1 - s2).add(v3.mul(s2));


            if (x1 > x2) {
                double tmp = x2;
                x2 = x1;
                x1 = tmp;

                pom = v12;
                v12 = v13;
                v13 = pom;
            }


            for (int x = (int) Math.max(x1 + 1, 0); x <= Math.min(x2, width - 1); x++) {

                double inter = (x - x1) / (x2 - x1);
                Vertex v = v12.mul(1 - inter).add(v13.mul(inter));
                zBufferVisibility.drawPixelWithTest(x, y, v.getZ(), shader.shade(v));
            }
        }
        for (int y = (int) Math.max(b.getY() + 1, 0); y <= Math.min(c.getY(), height - 1); y++) {

            double s2 = (y - a.getY()) / (c.getY() - a.getY());
            double x2 = (a.getX() * (1 - s2) + c.getX() * s2);

            Vertex v13 = v1.mul(1 - s2).add(v3.mul(s2));

            double s1 = (y - b.getY()) / (c.getY() - b.getY());
            double x1 = (b.getX() * (1 - s1) + c.getX() * s1);

            Vertex v23 = v2.mul(1 - s1).add(v3.mul(s1));

            if (x1 > x2) {
                double tmp = x2;
                x2 = x1;
                x1 = tmp;

                pom = v13;
                v13 = v23;
                v23 = pom;
            }

            for (int x = (int) Math.max(x1 + 1, 0); x <= Math.min(x2, width - 1); x++) {
                double inter = (x - x1) / (x2 - x1);

                Vertex v = v23.mul(1 - inter).add(v13.mul(inter));

                zBufferVisibility.drawPixelWithTest(x, y, v.getZ(), shader.shade(v));
            }
        }
    }
}
