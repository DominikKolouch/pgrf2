package rasterize;

import model.Vertex;
import raster.ZBufferVisibility;
import shaders.Shader;
import transforms.Vec3D;


public class LineRasterizer extends Rasterizer {


    public LineRasterizer(ZBufferVisibility zBufferVisibility, Shader shader) {
        super(zBufferVisibility, shader);
    }

    public void rasterizer(Vertex ver1, Vertex ver2) {

        ver1 = ver1.mul(1 / ver1.getW());
        ver2 = ver2.mul(1 / ver2.getW());

        Vec3D v1 = transToWindows(ver1);
        Vec3D v2 = transToWindows(ver2);

        int y1 = (int) v1.getY();
        int y2 = (int) v2.getY();
        int x1 = (int) v1.getX();
        int x2 = (int) v2.getX();
        int a, b;
        float k = (y2 - y1) / (float) (x2 - x1);
        float q = y1 - k * x1;

        if (Math.abs(y2 - y1) < Math.abs(x2 - x1)) {
            if (x2 < x1) {
                a = x1;
                x1 = x2;
                x2 = a;

                Vertex temp = ver1;
                ver1 = ver2;
                ver2 = temp;
            }
            for (int x = x1; x <= x2; x++) {
                float y = k * (float) x + q;
                double t = (x - x1) / (double) (x2 - x1);
                Vertex v = ver1.mul(1 - t).add(ver2.mul(t));
                zBufferVisibility.drawPixelWithTest(x, (int) y, v.getZ(), v.getColor());
            }
        }
        if (Math.abs(y2 - y1) >= Math.abs(x2 - x1)) {
            if (y2 < y1) {
                b = y1;
                y1 = y2;
                y2 = b;

                Vertex temp = ver1;
                ver1 = ver2;
                ver2 = temp;
            }
            for (int y = y1; y <= y2; y++) {
                float x = 0;
                if (x2 != x1) x = ((float) y - q) / k;
                if (x2 == x1)
                    x = x1;
                double t = (y - y1) / (double) (y2 - y1);
                Vertex v = ver1.mul(1 - t).add(ver2.mul(t));
                zBufferVisibility.drawPixelWithTest((int) x, y, v.getZ(), v.getColor());
            }
        }
    }
}