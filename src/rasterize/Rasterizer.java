package rasterize;

import model.Vertex;
import raster.ZBufferVisibility;
import shaders.Shader;
import transforms.Vec3D;

public abstract class Rasterizer {
    protected int width, height;
    protected Shader shader;
    ZBufferVisibility zBufferVisibility;

    public Rasterizer(ZBufferVisibility zBufferVisibility, Shader shader) {
        this.zBufferVisibility = zBufferVisibility;
        width = zBufferVisibility.getiBuffer().getWidth();
        height = zBufferVisibility.getiBuffer().getHeight();
        this.shader = shader;
    }

    public void rasterizer(Vertex v1, Vertex v2, Vertex v3) {

    }

    public void rasterizer(Vertex v1, Vertex v2) {

    }

    public void rasterizer(Vertex v1) {

    }

    public void setShader(Shader shader) {
        this.shader = shader;
    }


    protected Vec3D transToWindows(Vertex v) {
        return v.getPosition().ignoreW()
                .mul(new Vec3D(1, -1, 1))
                .add(new Vec3D(1, 1, 0))
                .mul(new Vec3D((width - 1) / 2.0, (height - 1) / 2.0, 1));
    }
}
