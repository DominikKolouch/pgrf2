package model;

import transforms.Col;
import transforms.Point3D;
import transforms.Vec2D;

import java.util.Optional;

public class Vertex {

    private final Point3D position;
    private Col color;
    private final Vec2D textCoord;
    private final double one;

    public Vertex(double x, double y, double z, Col color) {
        this.position = new Point3D(x, y, z);
        this.color = color;
        this.textCoord = new Vec2D(color.getR(), color.getB());
        this.one = 1.0;
    }

    public Vertex(Point3D p, Col color, Vec2D textCoord) {
        this.position = p;
        this.color = color;
        this.textCoord = textCoord;
        this.one = 1.0;
    }

    public Vertex(Point3D p, Col color, Vec2D textCoord, double one) {
        this.position = p;
        this.color = color;
        this.textCoord = textCoord;
        this.one = one;
    }


    public Point3D getPosition() {
        return position;
    }

    public Col getColor() {
        return color;
    }

    public void setColor(Col col) {
        this.color = col;
    }

    public Vec2D getTextCoord() {
        return textCoord;
    }

    public double getW() {
        return position.getW();
    }


    public Vertex mul(double d) {
        return new Vertex(position.mul(d), color.mul(d).saturate(), textCoord.mul(d), one * d);
    }


    public Vertex add(Vertex v) {
        return new Vertex(position.add(v.getPosition()),
                new Col(color.getR() + v.getColor().getR(), color.getG() + v.getColor().getG(), color.getB() + v.getColor().getB()),
                textCoord.add(v.textCoord), one + v.one);
    }


    public Optional<Vertex> dehomog() {
        if (position.getW() == 0) return Optional.empty();

        /*double x = position.getX();
        double y = position.getY();
        double z = position.getZ();*/
        double w = position.getW();

        Point3D dehomogPoint = new Point3D(position.dehomog().get());

        Col dehomCol = color.mul(1 / w);
        //Vec2D dehomogTextCoord = new Vec2D(textCoord.getX()/w,textCoord.getY()/w);

        double dehomogOne = one / w;


        return Optional.of(
                new Vertex(dehomogPoint, color, textCoord, one));
    }


    @Override
    public String toString() {
        return "Vertex{" +
                "position=" + position +
                ", color=" + color +
                '}';
    }

    public double getZ() {
        return position.getZ();
    }
}
