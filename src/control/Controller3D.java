package control;

import raster.ImageBuffer;
import raster.ZBufferVisibility;
import rasterize.LineRasterizer;
import rasterize.PointRasterizer;
import rasterize.Rasterizer;
import rasterize.TriangleRasterizer;
import render.Renderer;
import shaders.Shader;
import solids.*;
import transforms.*;
import view.Panel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;


public class Controller3D implements Controller {

    private final Panel panel;
    Renderer renderer;
    private Rasterizer triangleRasterizer;
    private Rasterizer lineRasterizer;
    private Rasterizer pointRasterizer;
    private ZBufferVisibility zBufferVisibility;
    private Point mousePos = new Point();
    private List<Shader> shaders = new ArrayList<>();
    private List<Solid> solids = new ArrayList<>();
    private Mat4 viewMatrix = new Mat4Identity();
    private Mat4 projection = new Mat4Identity();
    private Camera camera;
    private Solid axis;
    private Solid cube;
    private Solid diamond;
    private Solid grid;
    private boolean objectControl = false;
    private int activeSolid = 0;
    private int activeAxis = 0;
    private int activeShader = 0;
    private boolean persp = true;
    private boolean animation = false;
    private boolean isWired = false;
    private Thread thread;


    private int width, height;


    public Controller3D(Panel panel) {
        this.panel = panel;

        axis = new Axis();
        cube = new Cube();
        diamond = new Diamond();
        grid = new Grid(0.01);
        solids.add(grid);
        solids.add(cube);
        solids.add(diamond);

        camera = new Camera()
                .withPosition(new Vec3D(5, 0, 0))
                .withAzimuth(Math.PI)
                .withZenith(0)
                .withFirstPerson(true);

        initObjects(panel.getRaster());
        initListeners(panel);
        initShaders();


        triangleRasterizer = new TriangleRasterizer(zBufferVisibility, shaders.get(0));
        lineRasterizer = new LineRasterizer(zBufferVisibility, shaders.get(0));
        pointRasterizer = new PointRasterizer(zBufferVisibility, shaders.get(0));

        renderer = new Renderer();
        renderer.setTriangleRasterizer(triangleRasterizer);
        renderer.setLineRasterizer(lineRasterizer);
        renderer.setPointRasterizer(pointRasterizer);

        redraw();
    }

    public void initObjects(ImageBuffer raster) {
        raster.setClearValue(new Col(0x101010));

        zBufferVisibility = new ZBufferVisibility(raster);
        width = raster.getWidth();
        height = raster.getHeight();


    }

    @Override
    public void initListeners(Panel panel) {
        panel.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                mousePos = e.getPoint();
            }
        });

        panel.addMouseMotionListener(new MouseMotionAdapter() {
            public void mouseDragged(MouseEvent e) {
                double azimut = Math.PI * ((float) (mousePos.x - e.getX()) / width);
                double zenit = Math.PI * ((float) (mousePos.y - e.getY()) / height);

                if (zenit > 90) zenit = 90;
                else if (zenit < -90) zenit = -90;
                if (azimut > 90) azimut = 90;
                else if (azimut < -90) azimut = -90;

                camera = camera.addAzimuth(azimut);
                camera = camera.addZenith(zenit);

                hardClear();
                redraw();

                mousePos = e.getPoint();

            }
        });

        panel.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent key) {
                switch (key.getKeyCode()) {
                    case KeyEvent.VK_A -> moveLeft();
                    case KeyEvent.VK_D -> moveRight();
                    case KeyEvent.VK_W -> moveUp();
                    case KeyEvent.VK_S -> moveDown();
                    case KeyEvent.VK_Q -> moveFoward();
                    case KeyEvent.VK_E -> moveBackward();
                    case KeyEvent.VK_P -> persp = !persp;
                    case KeyEvent.VK_V -> isWired = !isWired;
                    case KeyEvent.VK_N -> handleActiveSolid();
                    case KeyEvent.VK_M -> activeAxis = ++activeAxis % 4;
                    case KeyEvent.VK_C -> objectControl = !objectControl;
                    case KeyEvent.VK_T -> changeShader();
                    case KeyEvent.VK_R -> animation();
                    case KeyEvent.VK_I -> showInfoMessage();
                    case 109 -> shrinkActiveModel();    //+
                    case 107 -> expandActiveModel();    //-
                    case 106 -> rotate();               //*
                }

                hardClear();
                redraw();
            }
        });

        panel.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                panel.resize();
                viewMatrix = camera.getViewMatrix();
                initObjects(panel.getRaster());
            }
        });
    }

    private void redraw() {
        panel.clear();
        width = panel.getRaster().getWidth();
        height = panel.getRaster().getHeight();
        panel.repaint();

        zBufferVisibility.clearDepthBuffer();
        viewMatrix = camera.getViewMatrix();

        if (persp) {
            projection = new Mat4PerspRH(20, (float) height / width, 0.1, 50);
        } else {
            projection = new Mat4OrthoRH(6, 4, 0.1, 50);

        }
        renderer.setProjection(projection);
        renderer.setView(viewMatrix);

        solids.forEach(x -> {
            if (x.Active) renderer.Render(x, isWired);
        });

        renderer.Render(axis, isWired);


    }

    private void hardClear() {
        panel.clear();

    }


    /*--------------------------------------OVLÁDACÍ METODY----------------------------------------*/
    private void handleActiveSolid() {
        activeSolid = ++activeSolid % 4;
        cube.Active = false;
        diamond.Active = false;
        grid.Active = false;
        switch (activeSolid) {
            case 0 -> {
                grid.Active = true;
            }
            case 1 -> {
                cube.Active = true;
            }
            case 2 -> {
                diamond.Active = true;
            }
            case 3 -> {
                cube.Active = true;
                diamond.Active = true;
                grid.Active = true;
            }

        }
    }

    private void initShaders() {
        Shader shaderGreenColor = v -> {
            return new Col(0, 1.0, 0);
        };
        Shader shaderRedColor = v -> {
            return new Col(1.0, 0, 0);
        };
        Shader shaderBlueColor = v -> {
            return new Col(0, 0, 1.0);
        };
        Shader shaderInterpolation = v -> {
            return v.getColor();
        };

        shaders.add(shaderGreenColor);
        shaders.add(shaderRedColor);
        shaders.add(shaderBlueColor);
        shaders.add(shaderInterpolation);
    }

    private void changeShader() {
        activeShader = ++activeShader % 4;
        triangleRasterizer.setShader(shaders.get(activeShader));
        lineRasterizer.setShader(shaders.get(activeShader));
    }

    private void animation() {
        animation = !animation;

        thread = new Thread(() -> {
            while (animation) {
                switch (activeAxis) {
                    case 0 -> {
                        for (Solid solid : solids) {
                            if (solid.Active)
                                solid.setModel(solid.getModel().mul(new Mat4RotX(0.01)));
                        }
                    }
                    case 1 -> {
                        for (Solid solid : solids) {
                            if (solid.Active)
                                solid.setModel(solid.getModel().mul(new Mat4RotY(0.01)));
                        }
                    }
                    case 2 -> {
                        for (Solid solid : solids) {
                            if (solid.Active)
                                solid.setModel(solid.getModel().mul(new Mat4RotZ(0.01)));
                        }
                    }
                }

                panel.clear();
                panel.repaint();
                redraw();
                try {
                    Thread.sleep(100);
                } catch (InterruptedException ex) {
                }
            }
        });
        thread.start();
    }

    private void showInfoMessage() {
        String info = """
                ovládání kamery QWEASD
                Přepnutí z kontroly kamery na kontrolu objektu: c -> Ovladaní objektu: QWESAD
                Změna pohledu kamery: p (Persp./Orth).
                Výběr aktivního objektu: m
                Zvětšení/zmenšení aktivního objektu: +/-
                Aktivace animace: r
                Změna rotace objektů: n
                Drátěný model: v
                Změna shaderu: t
                Rotace objektu *""";
        JOptionPane.showMessageDialog(null, info, "Informace", JOptionPane.INFORMATION_MESSAGE);
    }


    private void moveLeft() {
        if (objectControl) {
            for (Solid solid : solids) {
                if (solid.Active)
                    solid.setModel(solid.getModel().mul(new Mat4Transl(1.0, 0.0, 0.0)));
            }
        } else camera = camera.left(0.1);
    }

    private void moveRight() {
        if (objectControl) {
            for (Solid solid : solids) {
                if (solid.Active)
                    solid.setModel(solid.getModel().mul(new Mat4Transl(-1.0, 0.0, 0.0)));
            }
        } else camera = camera.right(0.1);
    }

    private void moveUp() {
        if (objectControl) {
            for (Solid solid : solids) {
                if (solid.Active)
                    solid.setModel(solid.getModel().mul(new Mat4Transl(0.0, 0.0, 1.0)));
            }
        } else camera = camera.up(0.1);
    }

    private void moveDown() {
        if (objectControl) {
            for (Solid solid : solids) {
                if (solid.Active)
                    solid.setModel(solid.getModel().mul(new Mat4Transl(0.0, 0.0, -1.0)));
            }
        } else camera = camera.down(0.1);
    }

    private void moveFoward() {
        if (objectControl) {
            for (Solid solid : solids) {
                if (solid.Active)
                    solid.setModel(solid.getModel().mul(new Mat4Transl(0.0, 1.0, 0.0)));
            }
        } else camera = camera.forward(0.1);
    }

    private void moveBackward() {
        if (objectControl) {
            for (Solid solid : solids) {
                if (solid.Active)
                    solid.setModel(solid.getModel().mul(new Mat4Transl(0.0, -1.0, 0.0)));
            }
        } else camera = camera.backward(0.1);
    }

    private void expandActiveModel() {
        for (Solid solid : solids) {
            if (solid.Active)
                solid.setModel(solid.getModel().mul(new Mat4Scale(1.01)));
        }
    }

    private void shrinkActiveModel() {
        for (Solid solid : solids) {
            if (solid.Active)
                solid.setModel(solid.getModel().mul(new Mat4Scale(1.01)));
        }
    }

    private void rotate() {
        switch (activeAxis) {
            case 0 -> {
                for (Solid solid : solids) {
                    if (solid.Active)
                        solid.setModel(solid.getModel().mul(new Mat4RotX(0.01)));
                }
            }
            case 1 -> {
                for (Solid solid : solids) {
                    if (solid.Active)
                        solid.setModel(solid.getModel().mul(new Mat4RotY(0.01)));
                }
            }
            case 2 -> {
                for (Solid solid : solids) {
                    if (solid.Active)
                        solid.setModel(solid.getModel().mul(new Mat4RotZ(0.01)));
                }
            }
        }
    }

}
