package solids;

import model.Part;
import model.TopologyType;
import model.Vertex;
import transforms.Col;

public class Cube extends Solid {

    public Cube() {
        double cube = 0.5;
        vertexBuffer.add(new Vertex(0, 0, 0, new Col(0xff0000)));  //0
        vertexBuffer.add(new Vertex(cube, 0, 0, new Col(0x00ff00)));  //1
        vertexBuffer.add(new Vertex(cube, cube, 0, new Col(0x0000ff)));  //2
        vertexBuffer.add(new Vertex(0, cube, 0, new Col(0x0000ff)));  //3

        vertexBuffer.add(new Vertex(0, 0, cube, new Col(0x00ff00)));  //4
        vertexBuffer.add(new Vertex(cube, 0, cube, new Col(0xff0000)));  //5
        vertexBuffer.add(new Vertex(cube, cube, cube, new Col(0x00ff00)));  //6
        vertexBuffer.add(new Vertex(0, cube, cube, new Col(0x0000ff)));  //7


        indexBuffer.add(0);
        indexBuffer.add(1);
        indexBuffer.add(3);

        indexBuffer.add(1);
        indexBuffer.add(2);
        indexBuffer.add(3);

        indexBuffer.add(1);
        indexBuffer.add(2);
        indexBuffer.add(5);

        indexBuffer.add(5);
        indexBuffer.add(2);
        indexBuffer.add(6);

        indexBuffer.add(4);
        indexBuffer.add(7);
        indexBuffer.add(0);

        indexBuffer.add(0);
        indexBuffer.add(7);
        indexBuffer.add(3);

        indexBuffer.add(6);
        indexBuffer.add(5);
        indexBuffer.add(4);

        indexBuffer.add(4);
        indexBuffer.add(6);
        indexBuffer.add(7);

        indexBuffer.add(7);
        indexBuffer.add(2);
        indexBuffer.add(3);

        indexBuffer.add(6);
        indexBuffer.add(2);
        indexBuffer.add(7);

        indexBuffer.add(0);
        indexBuffer.add(1);
        indexBuffer.add(4);

        indexBuffer.add(1);
        indexBuffer.add(5);
        indexBuffer.add(4);

        partBuffer.add(new Part(TopologyType.TRIANGLES, 0, 12));
    }
}
