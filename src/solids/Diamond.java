package solids;

import model.Part;
import model.TopologyType;
import model.Vertex;
import transforms.Col;

public class Diamond extends Solid {

    private final double diamond = 0.3;
    private final double vrch = 0.8;

    public Diamond() {
        //vrch -> y zůstává z,x se mění
        vertexBuffer.add(new Vertex(-diamond, vrch, 0, new Col(0x00ff00))); //0
        vertexBuffer.add(new Vertex(0, vrch, diamond, new Col(0xFEBA03))); //1
        vertexBuffer.add(new Vertex(diamond, vrch, diamond, new Col(0x00ff00))); //2
        vertexBuffer.add(new Vertex(2 * diamond, vrch, 0, new Col(0xFEBA03))); //3

        vertexBuffer.add(new Vertex(-diamond, vrch, -diamond, new Col(0x00ff00))); //4
        vertexBuffer.add(new Vertex(0, vrch, -2 * diamond, new Col(0xFEBA03))); //5
        vertexBuffer.add(new Vertex(diamond, vrch, -2 * diamond, new Col(0x00ff00))); //6
        vertexBuffer.add(new Vertex(2 * diamond, vrch, -diamond, new Col(0xFEBA03))); //7

        //střed vrchu
        vertexBuffer.add(new Vertex(diamond / 2, vrch, -diamond / 2, new Col(0xff0000))); //8
        //trojuhelník pod každou linkou diamantu //y všude stejné
        vertexBuffer.add(new Vertex(diamond / 2, vrch - diamond / 2, 1.5 * diamond, new Col(0x0000ff))); //9 mezi 1-2
        vertexBuffer.add(new Vertex(2.5 * diamond, vrch - diamond / 2, -0.5 * diamond, new Col(0x0000ff))); //10 mezi 3 a 7
        vertexBuffer.add(new Vertex(-1.5 * diamond, vrch - diamond / 2, -0.5 * diamond, new Col(0x0000ff))); //11 mezi 0 a 4
        vertexBuffer.add(new Vertex(diamond / 2, vrch - diamond / 2, -2.5 * diamond, new Col(0x0000ff))); //12 mezi 5-6
        vertexBuffer.add(new Vertex(diamond * 2, vrch - diamond / 2, diamond, new Col(0x0000ff))); //13 mezi 2-3
        vertexBuffer.add(new Vertex(diamond * 2, vrch - diamond / 2, -2 * diamond, new Col(0x0000ff))); //14 mezi 6-7
        vertexBuffer.add(new Vertex(-diamond, vrch - diamond / 2, -2 * diamond, new Col(0x0000ff))); //15 mezi 4-5
        vertexBuffer.add(new Vertex(-diamond, vrch - diamond / 2, diamond, new Col(0x0000ff))); //16 mezi 0-1
        //špička
        vertexBuffer.add(new Vertex(diamond / 2, 0, -diamond / 2, new Col(0xff0000))); //17

        indexBuffer.add(0);
        indexBuffer.add(1);
        indexBuffer.add(8);

        indexBuffer.add(8);
        indexBuffer.add(1);
        indexBuffer.add(2);

        indexBuffer.add(8);
        indexBuffer.add(2);
        indexBuffer.add(3);

        indexBuffer.add(8);
        indexBuffer.add(7);
        indexBuffer.add(3);

        indexBuffer.add(8);
        indexBuffer.add(7);
        indexBuffer.add(6);

        indexBuffer.add(8);
        indexBuffer.add(5);
        indexBuffer.add(6);

        indexBuffer.add(8);
        indexBuffer.add(4);
        indexBuffer.add(5);

        indexBuffer.add(8);
        indexBuffer.add(4);
        indexBuffer.add(0);

        indexBuffer.add(1);
        indexBuffer.add(2);
        indexBuffer.add(9);

        indexBuffer.add(10);
        indexBuffer.add(3);
        indexBuffer.add(7);

        indexBuffer.add(11);
        indexBuffer.add(0);
        indexBuffer.add(4);

        indexBuffer.add(12);
        indexBuffer.add(5);
        indexBuffer.add(6);

        indexBuffer.add(13);
        indexBuffer.add(2);
        indexBuffer.add(3);

        indexBuffer.add(14);
        indexBuffer.add(7);
        indexBuffer.add(6);

        indexBuffer.add(15);
        indexBuffer.add(4);
        indexBuffer.add(5);

        indexBuffer.add(16);
        indexBuffer.add(0);
        indexBuffer.add(1);

        indexBuffer.add(16);
        indexBuffer.add(9);
        indexBuffer.add(17);

        indexBuffer.add(13);
        indexBuffer.add(9);
        indexBuffer.add(17);

        indexBuffer.add(13);
        indexBuffer.add(10);
        indexBuffer.add(17);

        indexBuffer.add(10);
        indexBuffer.add(14);
        indexBuffer.add(17);

        indexBuffer.add(14);
        indexBuffer.add(12);
        indexBuffer.add(17);

        indexBuffer.add(12);
        indexBuffer.add(15);
        indexBuffer.add(17);

        indexBuffer.add(15);
        indexBuffer.add(11);
        indexBuffer.add(17);

        indexBuffer.add(11);
        indexBuffer.add(16);
        indexBuffer.add(17);

        //24 - 25.
        indexBuffer.add(11);
        indexBuffer.add(0);
        indexBuffer.add(16);

        indexBuffer.add(1);
        indexBuffer.add(16);
        indexBuffer.add(9);

        indexBuffer.add(2);
        indexBuffer.add(13);
        indexBuffer.add(9);

        indexBuffer.add(13);
        indexBuffer.add(3);
        indexBuffer.add(10);

        indexBuffer.add(10);
        indexBuffer.add(7);
        indexBuffer.add(14);

        indexBuffer.add(14);
        indexBuffer.add(6);
        indexBuffer.add(12);

        indexBuffer.add(12);
        indexBuffer.add(5);
        indexBuffer.add(15);

        indexBuffer.add(11);
        indexBuffer.add(16);
        indexBuffer.add(17);

        indexBuffer.add(15);
        indexBuffer.add(4);
        indexBuffer.add(11);


        partBuffer.add(new Part(TopologyType.TRIANGLES, 0, 33));
    }
}
