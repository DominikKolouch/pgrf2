package solids;

import model.Part;
import model.TopologyType;
import model.Vertex;
import transforms.Col;

import java.awt.*;

public class Axis extends Solid {

    public Axis() {
        //střed
        vertexBuffer.add(new Vertex(0, 0, 0, new Col(Color.yellow.getRGB()))); //0
        //x
        vertexBuffer.add(new Vertex(0.8, 0, 0, new Col(Color.blue.getRGB()))); //1
        //y
        vertexBuffer.add(new Vertex(0, 0.8, 0, new Col(Color.red.getRGB()))); //2
        //z
        vertexBuffer.add(new Vertex(0, 0, 0.8, new Col(Color.green.getRGB()))); //3


        //x axis arrow
        vertexBuffer.add(new Vertex(0.8, 0.05, -0.05, new Col(Color.cyan.getRGB()))); //4
        vertexBuffer.add(new Vertex(0.8, -0.05, -0.05, new Col(Color.cyan.getRGB()))); //5
        //hrot
        vertexBuffer.add(new Vertex(0.85, 0.00, 0, new Col(Color.white.getRGB()))); //6

        vertexBuffer.add(new Vertex(0.8, 0.05, 0.05, new Col(Color.cyan.getRGB()))); //7
        vertexBuffer.add(new Vertex(0.8, -0.05, 0.05, new Col(Color.cyan.getRGB()))); //8

        //y axis arrow
        vertexBuffer.add(new Vertex(0.05, 0.8, 0.05, new Col(Color.blue.getRGB()))); //9
        vertexBuffer.add(new Vertex(-0.05, 0.8, 0.05, new Col(Color.blue.getRGB()))); //10
        //hrot
        vertexBuffer.add(new Vertex(0, 0.85, 0, new Col(Color.white.getRGB()))); //11

        vertexBuffer.add(new Vertex(0.05, 0.8, -0.05, new Col(Color.blue.getRGB()))); //12
        vertexBuffer.add(new Vertex(-0.05, 0.8, -0.05, new Col(Color.blue.getRGB()))); //13

        //z axis arrow
        vertexBuffer.add(new Vertex(-0.05, -0.05, 0.80, new Col(Color.green.getRGB())));//14
        vertexBuffer.add(new Vertex(0.05, -0.05, 0.80, new Col(Color.green.getRGB())));//15
        //hrot
        vertexBuffer.add(new Vertex(0, 0, 0.85, new Col(Color.white.getRGB())));//16

        vertexBuffer.add(new Vertex(-0.05, 0.05, 0.80, new Col(Color.green.getRGB())));//17
        vertexBuffer.add(new Vertex(0.05, 0.05, 0.80, new Col(Color.green.getRGB())));//18


        indexBuffer.add(4);
        indexBuffer.add(5);
        indexBuffer.add(6);

        indexBuffer.add(6);
        indexBuffer.add(7);
        indexBuffer.add(8);

        indexBuffer.add(6);
        indexBuffer.add(7);
        indexBuffer.add(4);

        indexBuffer.add(5);
        indexBuffer.add(6);
        indexBuffer.add(8);

        indexBuffer.add(9);
        indexBuffer.add(10);
        indexBuffer.add(11);

        indexBuffer.add(13);
        indexBuffer.add(12);
        indexBuffer.add(11);

        indexBuffer.add(10);
        indexBuffer.add(13);
        indexBuffer.add(11);

        indexBuffer.add(11);
        indexBuffer.add(12);
        indexBuffer.add(9);

        indexBuffer.add(14);
        indexBuffer.add(15);
        indexBuffer.add(16);

        indexBuffer.add(16);
        indexBuffer.add(17);
        indexBuffer.add(18);

        indexBuffer.add(14);
        indexBuffer.add(16);
        indexBuffer.add(17);

        indexBuffer.add(15);
        indexBuffer.add(16);
        indexBuffer.add(18);


        indexBuffer.add(0);
        indexBuffer.add(1);

        indexBuffer.add(0);
        indexBuffer.add(2);

        indexBuffer.add(0);
        indexBuffer.add(3);


        partBuffer.add(new Part(TopologyType.TRIANGLES, 0, 12));
        partBuffer.add(new Part(TopologyType.LINES, 36, 3));

    }
}
