package solids;

import model.Part;
import model.TopologyType;
import model.Vertex;
import transforms.Bicubic;
import transforms.Col;
import transforms.Cubic;
import transforms.Point3D;

public class Grid extends Solid {

    public Grid(double smooth) {

        int rada = 0;
        Bicubic bezierBicubic = new Bicubic(Cubic.BEZIER,
                new Point3D(-2.0, 5.0, 3.0), new Point3D(-2.0, 3.0, 2.0), new Point3D(-2.0, 1.0, 7.0), new Point3D(-2.0, 2.0, 4.0),
                new Point3D(0.0, 2.0, 3.0), new Point3D(0.0, 2.0, 4.0), new Point3D(0.0, 0.0, 4.0), new Point3D(0.0, 2.0, 1.0),
                new Point3D(1.0, 5.0, 4.0), new Point3D(1.0, 5.0, 2.0), new Point3D(1.0, 2.0, 3.0), new Point3D(1.0, 1.0, 4.0),
                new Point3D(2.0, 4.0, 4.0), new Point3D(2.0, 3.0, 6.0), new Point3D(2.0, 2.0, 3.0), new Point3D(2.0, 2.0, 3.0));


        for (double i = 0.0; i < 1.0; i += smooth) {
            rada++;
            for (double j = 0.0; j < 1.0; j += smooth)
                getVertexBuffer().add(new Vertex(bezierBicubic.compute(i, j).getX(), bezierBicubic.compute(i, j).getY(), bezierBicubic.compute(i, j).getZ(), new Col(0xf0f0f0)));
        }
        for (int i = 0; i < rada; i++) {
            for (int j = 0; j < rada; j++) {
                if (i < rada - 1) {
                    indexBuffer.add(i * rada + j);
                    indexBuffer.add((i + 1) * rada + j);
                }
                if (j < rada - 1) {
                    indexBuffer.add(i * rada + j);
                    indexBuffer.add(i * rada + j + 1);
                }
            }
        }

        getPartBuffer().add(new Part(TopologyType.LINES, 0, indexBuffer.size() / 2));

    }

}