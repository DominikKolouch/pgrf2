package render;

import clipper.Clipper;
import model.Part;
import model.Vertex;
import rasterize.Rasterizer;
import solids.Solid;
import transforms.Mat4;
import transforms.Mat4Identity;

import java.util.*;

public class Renderer {
    private Rasterizer triangleRasterizer;
    private Rasterizer lineRasterizer;
    private Rasterizer pointRasterizer;
    private Mat4 view = new Mat4Identity(), proj = new Mat4Identity();
    private final Clipper clipper;

    public Renderer() {
        clipper = new Clipper();
    }

    public void setTriangleRasterizer(Rasterizer rasterizer) {
        this.triangleRasterizer = rasterizer;
    }

    public void setLineRasterizer(Rasterizer rasterizer) {
        this.lineRasterizer = rasterizer;
    }

    public void setPointRasterizer(Rasterizer rasterizer) {
        this.pointRasterizer = rasterizer;
    }


    public void setView(Mat4 view) {
        this.view = view;
    }

    public void setProjection(Mat4 proj) {
        this.proj = proj;
    }


    public void Render(Solid solid, boolean isWired) {
        List<Vertex> tempVB = new ArrayList<>();
        Mat4 trans = solid.getModel().mul(view).mul(proj);
        for (Vertex vertex : solid.getVertexBuffer()) {
            vertex = new Vertex(vertex.getPosition().mul(trans), vertex.getColor(), vertex.getTextCoord());
            tempVB.add(vertex);
        }


        for (Part p : solid.getPartBuffer()) {
            switch (p.getType()) {
                case POINTS -> {
                    int start = p.getStartIndex();
                    for (int i = 0; i < p.getCount(); i++) {

                        int indexV1 = start;
                        start += 1;

                        Vertex v1 = tempVB.get(solid.getIndexBuffer().get(indexV1));
                        if (clipper.clipPoint(v1)) return;
                        pointRasterizer.rasterizer(v1);
                    }
                }
                case LINES -> {
                    int start = p.getStartIndex();
                    for (int i = 0; i < p.getCount(); ++i) {
                        int indexV1 = start;
                        int indexV2 = start + 1;

                        start += 2;

                        Vertex v1 = tempVB.get(solid.getIndexBuffer().get(indexV1));
                        Vertex v2 = tempVB.get(solid.getIndexBuffer().get(indexV2));

                        renderLine(v1, v2);
                    }
                }

                case LINES_STRIP -> {
                    int start = p.getStartIndex();
                    for (int i = 0; i < p.getCount(); ++i) {
                        int indexV1 = start;
                        int indexV2 = start + 1;

                        start += 1;

                        Vertex v1 = tempVB.get(solid.getIndexBuffer().get(indexV1));
                        Vertex v2 = tempVB.get(solid.getIndexBuffer().get(indexV2));

                        renderLine(v1, v2);
                    }
                }
                case TRIANGLES_STRIP -> {
                    int start = p.getStartIndex();
                    for (int i = 0; i < p.getCount(); ++i) {
                        int indexV1 = start;
                        int indexV2 = start + 1;
                        int indexV3 = start + 2;

                        start += 1;

                        Vertex v1 = tempVB.get(solid.getIndexBuffer().get(indexV1));
                        Vertex v2 = tempVB.get(solid.getIndexBuffer().get(indexV2));
                        Vertex v3 = tempVB.get(solid.getIndexBuffer().get(indexV3));

                        if (!isWired) {
                            renderTriangle(v1, v2, v3);
                        } else {
                            renderLine(v1, v2);
                            renderLine(v3, v2);
                            renderLine(v1, v3);
                        }
                    }
                }

                case TRIANGLES -> {
                    int start = p.getStartIndex();
                    for (int i = 0; i < p.getCount(); ++i) {
                        int indexV1 = start;
                        int indexV2 = start + 1;
                        int indexV3 = start + 2;

                        start += 3;

                        Vertex v1 = tempVB.get(solid.getIndexBuffer().get(indexV1));
                        Vertex v2 = tempVB.get(solid.getIndexBuffer().get(indexV2));
                        Vertex v3 = tempVB.get(solid.getIndexBuffer().get(indexV3));

                        if (!isWired) {
                            renderTriangle(v1, v2, v3);
                        } else {
                            renderLine(v1, v2);
                            renderLine(v3, v2);
                            renderLine(v1, v3);
                        }
                    }

                }
            }
        }

    }

    public void Render(List<Solid> scene, boolean isWired) {
        for (Solid s : scene) {
            Render(s, isWired);
        }
    }


    private void renderTriangle(Vertex v1, Vertex v2, Vertex v3) {
        if (clipper.triangleClip(v1, v2, v3)) return;

        rasterizerTriangleZClip(v1, v2, v3);
    }

    private void renderLine(Vertex v1, Vertex v2) {
        if (clipper.lineClip(v1, v2)) return;

        rasterizeLineClip(v1, v2);
    }

    private void rasterizerTriangleZClip(Vertex v1, Vertex v2, Vertex v3) {

        List<Vertex> vec3DList = Arrays.asList(v1, v2, v3);
        Collections.sort(vec3DList, Comparator.comparingDouble(Vertex::getZ));
        v1 = vec3DList.get(2);
        v2 = vec3DList.get(1);
        v3 = vec3DList.get(0);


        if (v1.getPosition().getZ() < 0)
            return;

        if (v2.getPosition().getZ() < 0) {
            double t1 = 0 - v1.getPosition().getZ() / (v2.getPosition().getZ() - v1.getPosition().getZ());
            Vertex ab = v1.mul(1 - t1).add(v2.mul(t1));

            double t2 = 0 - v1.getPosition().getZ() / (v3.getPosition().getZ() - v1.getPosition().getZ());
            Vertex ac = v1.mul(1 - t2).add(v3.mul(t2));

            triangleRasterizer.rasterizer(v1, ab, ac);
            return;
        }
        if (v3.getPosition().getZ() < 0) {
            double t1 = 0 - v1.getPosition().getZ() / (v3.getPosition().getZ() - v1.getPosition().getZ());
            Vertex ac = v1.mul(1 - t1).add(v3.mul(t1));
            double t2 = 0 - v2.getPosition().getZ() / (v3.getPosition().getZ() - v2.getPosition().getZ());
            Vertex bc = v2.mul(1 - t2).add(v3.mul(t2));

            triangleRasterizer.rasterizer(v1, v2, bc);
            triangleRasterizer.rasterizer(v1, ac, bc);
            return;
        }
        triangleRasterizer.rasterizer(v1, v2, v3);

    }

    private void rasterizeLineClip(Vertex v1, Vertex v2) {
        if (v1.getPosition().getZ() < 0)
            return;

        if (v2.getPosition().getZ() < 0) {
            double t1 = v1.getPosition().getZ() / (v1.getPosition().getZ() - v2.getPosition().getZ());
            Vertex ab = v1.mul(1 - t1).add(v2.mul(t1));
            lineRasterizer.rasterizer(v1, ab);
            return;
        }

        lineRasterizer.rasterizer(v1, v2);
    }
}
